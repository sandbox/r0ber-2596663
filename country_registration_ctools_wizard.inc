<?php

/**
 * @file
 * Provides bunch of ctools wizards function for multi step form.
 * 
 */


/**
 * Comenzamos definiendoo un callback principal, donde definiremos los aspectos más imporatntes de nuestro formulario multi-step.
 * Definiremos los formularios que irán cargando y el orden en que lo harán, títulos, configuración etc..
 * Mas documentación aquí http://cgit.drupalcode.org/ctools/plain/help/wizard.html?id=refs/heads/7.x-1.x
 * 
 * Como podremos comprobar nuestro formulario tendrá:
 * 
 *   El formulario por defecto "registration form",
 *    el formulario "groups form",
 *   y por último el formulario "invite form"
 *
 * nota: cada paso del formulario es un formulario independiente.
 *
 * Create callback for standard ctools registration wizard.
* 
*/
function country_registration_ctools_wizard($step = 'register') {
    
  // Include required ctools files.
  ctools_include('wizard');
  
  // Our form save steps data in an object cache (not in db) !!important thing.
  ctools_include('object-cache');
  
  $form_info = array(
    // Specify unique form id for this form.
    'id' => 'multistep_registration',
    //Specify the path for this form. It is important to include space for the $step argument to be passed.
    'path' => "user/register/%step",
    // Show breadcrumb trail.
    'show trail' => TRUE,
    'show back' => TRUE,
    'show return' => TRUE,
    'next text' => t('Avanti'),
    // Callback to use when the 'next' button is clicked.
    'next callback' => 'country_registration_subtask_next',
    // Callback to use when entire form is completed.
    'finish callback' => 'country_registration_subtask_finish',
    // Callback to use when user clicks final submit button.
    'return callback' => 'country_registration_subtask_finish',
    // Callback to use when user cancels wizard.
    'cancel callback' => 'country_registration_subtask_cancel',
    // Specify the order that the child forms will appear in, as well as their page titles.
    'order' => array(
      'register' => t('Register'),
      'groups' => t('Connect'),
      'invite' => t('Invite'),
    ),
    // Define the child forms. Be sure to use the same keys here that were user in the 'order' section of this array.
    'forms' => array(
      'register' => array(
        'form id' => 'user_register_form'
      ),
      'groups' => array(
        'form id' => 'country_registration_group_info_form',
        // Be sure to load the required include file if the form callback is not defined in the .module file.
        'include' => drupal_get_path('module', 'country_registration').'/country_registration_forms.inc',
      ),
      'invite' => array(
        'form id' => 'country_registration_invite_form',
        'include' => drupal_get_path('module', 'country_registration').'/country_registration_forms.inc',
      ),
    ),
  ); 
  
  // dsm(drupal_get_path('module', 'country_registration'), 'form load path');
  // Make cached data available within each step's $form_state array.
  // Hacemos que los datos de la cache estén diosponibles en cada paso del array $form_state.
  $form_state['signup_object'] = country_registration_get_page_cache('signup');

 
 // dsm($step, 'STEP');
 // Return the form as a Ctools multi-step form.
 // Devuelve el formulario como un formulario multistep Ctools.
  $output = ctools_wizard_multistep_form($form_info, $step, $form_state);
 
  //dsm($output, 'build output');

  return $output;

}

/**
* 
* Ctools almacena los datos de cada paso del formulario en un objecto llamado "Object Cache".
* Como veremos una vez completado el formulario usaremos todos los datos para el procesado·
* Recibe un objeto de la memoria cache.
* 
* @param string $name
*  The name of the cached object to retreive.
*
*/
function country_registration_get_page_cache($name) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_get('country_registration', $name);

 
// If the cached object doesn't exist yet, create an empty object.
  if (!$cache) {
    $cache = new stdClass();
    $cache->locked = ctools_object_cache_test('country_registration', $name);
  }

  return $cache;
}

/**
* Creates or updates an object in the cache.
*
* @param string $name
*  The name of the object to cache.
*
* @param object $data
*  The object to be cached.
*/
function country_registration_set_page_cache($name, $data) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_set('country_registration', $name, $data);
}

/**
* Removes an item from the object cache.
*
* @param string $name
*  The name of the object to destroy.
*/
function country_registration_clear_page_cache($name) {
  ctools_include('object-cache');
  ctools_object_cache_clear('country_registration', $name);
}

/**
 * Funciones callback que serán llamadas cuando el usuario haga click en "next", "cancel" o "finish".
 * 
 */

/**
* Callback executed when the 'next' button is clicked.
*/
function country_registration_subtask_next(&$form_state) {

  // Store submitted data in a ctools cache object, namespaced 'signup'.
  country_registration_set_page_cache('signup', $form_state['values']);
}

/**
* Callback executed when the 'cancel' button is clicked.
*/
function country_registration_subtask_cancel(&$form_state) {
  // Clear our ctools cache object. It's good housekeeping.
  country_registration_clear_page_cache('signup');
}

/**
* Callback executed when the entire form submission is finished.
*/
function country_registration_subtask_finish(&$form_state) {
  // Clear our Ctool cache object.
  country_registration_clear_page_cache('signup');

 
// Redirect the user to the front page.
  drupal_goto('<front>');
}